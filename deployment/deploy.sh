#!/usr/bin/env bash

export BITBUCKET_BRANCH=$1

echo "Deploying front-end branch $BITBUCKET_BRANCH via user $USER"

mkdir -p /home/epidemio/frontend/$BITBUCKET_BRANCH/
cd /home/epidemio/frontend/$BITBUCKET_BRANCH/

if cd windesitis-epidemo-engine-front-end; then git checkout $BITBUCKET_BRANCH && git pull; else git clone --single-branch --branch $BITBUCKET_BRANCH git@bitbucket.org:ictinnovaties-zorg/windesitis-epidemo-engine-front-end.git && cd windesitis-epidemo-engine-front-end; fi

docker-compose up -d  --force-recreate --build
