# epidemio-front-end

> Front-end of Epidemio Fighting Epidemics Application

## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

## Requirements
As visible in the `nuxt.config.js` this front-end requires a working back-end api and socket server. 
Please change the url's/hostnames in your `nuxt.config.js` according to the back-end you're using.


## Docker compose start for development 
```shell script
export NODE_ENV=development #unix based
set NODE_ENV=development #win based

docker-compose up -d --force-recreate --build
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
